
# WIBOL(Windows Iso Bootable On Linux)
A GUI application to create bootable USB of Windows ISO on Linux
## Note
At the time application can only create bootable usb for BIOS/Legacy systems

![](./assets/screenshot.png)
---
# Dependencies
## Runtime
- rsync
- sync
- ntfs-3g
- ms-sys
- parted
## Build
- fltk
- g++
---
# Compiling
```
git clone https://codeberg.org/Luciogi/wibol.git
cd wibol
g++ -lfltk wibol.cpp -o wibol
```
---
# How to Use?

1. Use following command to identify USB device name 
```bash
lsblk
```
Let say USB name is `sdc`

3. Execute **wibol** with root privillages

```bash
sudo ./wibol
```
4. Type USB in USB Field e.g `/dev/sdc`

### NOTE:
Make sure iso filename does not contain space.

5. Click on Select button to choose Windows Iso File

---
# Tested ISO
- Windows 7
- Windows 10
