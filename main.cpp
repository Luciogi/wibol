#include <FL/Fl.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Input.H>
#include <FL/Fl_Widget.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Output.H>
#include <FL/Fl_File_Input.H>
#include <FL/Fl_File_Chooser.H>
#include <FL/Fl_Choice.H>
#include <FL/fl_ask.H>

#include <iostream>
struct Input{
	Fl_Input		*usb;
	Fl_File_Input	*iso;
	Fl_Button		*selectFile;
	Fl_Button		*startBtn;
	bool 			 runScript;

	char isoPath[254];
	char usbPath[254];
};

void startScript (Input* parameter){
	Input * temp = (Input*)parameter;

	std::cerr << "Script exetcutiion\n";
	std::string usbPlaceHolder = temp->usbPath;
	std::string isoPlaceHolder = temp->isoPath;
	char usbPartition_num = '1';
	std::string cmds[] = {
		"parted -s "+usbPlaceHolder+" mklabel msdos",
		"parted -s "+usbPlaceHolder+" mkpart primary ntfs 1 100%",
		"parted -s "+usbPlaceHolder+" set "+usbPartition_num+" boot on",
		"mkfs.ntfs -f "+usbPlaceHolder+usbPartition_num,
		"ms-sys -7 "+usbPlaceHolder,
		"ms-sys -n "+usbPlaceHolder+usbPartition_num,
		"mkdir -p /mnt/iso",
		"mkdir -p /mnt/usb",
		"mount "+usbPlaceHolder+usbPartition_num+" /mnt/usb",
		"mount -o loop "+isoPlaceHolder+" /mnt/iso",
		"rsync -rvh /mnt/iso/ /mnt/usb",
		"sync /mnt/usb/sources/install.wim",
		"umount /mnt/iso",
		"umount /mnt/usb",
		"sleep 5",
		"umount /mnt/iso",
		"umount /mnt/usb",
		"rm -rf /mnt/usb",
		"rm -rf /mnt/iso"
	};

	for(int i = 0; i<=16; i++){
		std::cerr << cmds[i] << std::endl;
		std::system(cmds[i].c_str());
	}
	std::cerr << "Script Execution completed\n";

}
void startBtn_cb(Fl_Widget *widget, void *parameter){ // Call back function must be return void, and Fl_Widget must be used as parameter regardless of items
	Input *temp = (Input*) parameter;
	strcpy(temp->usbPath, temp->usb->value());
	strcpy(temp->isoPath, temp->iso->value());
	std::string isoPlaceHolder = temp->isoPath;
	std::string usbPlaceHolder = temp->usbPath;
	std::string warning = "Warning: All Data on "+usbPlaceHolder+" will be destroyed during this process";
	// std::string *warningPtr = &warning;
	switch( fl_choice(warning.c_str(),"Yes", "No", 0)) {
		case 0: {
			switch (fl_choice("Would you like to continue", "Yes", "No", 0)) {
				case 0:{
					temp->runScript = true;
					temp->startBtn->deactivate();
					break;
				}
				case 2:{
					break;
				}
			}
			break;
		}
		case 1: {
			break;
		}
	}
	Fl::wait();
	if(temp->runScript != false){
		startScript(temp);
		temp->startBtn->activate();
	}
}
void inputIsoFile_cb(Fl_Widget* w, void* p){
	Input *temp = (Input*) p;
	std::string directory;
	directory = getenv("HOME");
	char* filename = fl_file_chooser("Open File","*iso", directory.c_str());
	temp->iso->value(filename);
}

int main(int argc, char **argv) {
	Input input;

	// Unmount warning
	fl_alert("Please Unmount drive before using.");

	// Main window
	Fl_Window *window = new Fl_Window(500,700);
	window->label("WIBOL");
	int GlobalX = 50;

	// Title Box
	Fl_Box *box = new Fl_Box(GlobalX, 40, 400, 50, "Windows ISO Burner On Linux");
	box->box(FL_FLAT_BOX);
	box->labelfont(FL_BOLD);
	box->labelsize(22);
	box->labelcolor(FL_WHITE);
	box->color(FL_BLACK);

	// USB input field
	input.usb =  new Fl_Input(GlobalX+110, 100, 290, 30,"Select USB: ");
	input.usb->labelsize(20);

	// ISO file input field
	input.iso = new  Fl_File_Input(GlobalX+110, 150, 210, 40, "ISO:");
	input.iso->labelsize(20);

	// Select File Button
	input.selectFile = new Fl_Button(GlobalX+330, 150,70,40, "Select");
	input.selectFile->labelsize(20);
	input.selectFile->callback(inputIsoFile_cb,&input);

	// Start button
	input.startBtn = new Fl_Button(GlobalX+330, 600, 90, 30, " Start");
	input.startBtn->labelsize(20);
	input.startBtn->when(FL_WHEN_RELEASE);
	input.startBtn->callback(startBtn_cb, &input);


	window->end();
	window->show(argc, argv);
	return Fl::run();
}
